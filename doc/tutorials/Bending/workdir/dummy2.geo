// Gmsh project created on Wed Jul 05 15:01:47 2017
lc=0.5;

R=5.0;
cx=15.0;
cy=-5.0;

Point(1) = {cx, cy, 0, lc};
Point(2) = {cx+R, cy, 0, lc};
Circle(1) = {2, 1, 2};
Line Loop(1) = {1};
Plane Surface(1) = {1};
Recombine Surface {1};

Physical Line("SURFACE") = {1};
Physical Surface("ALL_ELEMENTS") = {1};