// Gmsh project created on Tue Jul 04 13:45:43 2017
lc=0.1111111111111111;

R=5.0;
cx=0;
cy=5.0;

Point(1) = {cx, cy, 0, lc};
Point(2) = {cx+R, cy, 0, lc};
Circle(1) = {2, 1, 2};
Line Loop(1) = {1};
Plane Surface(1) = {1};
Recombine Surface {1};

Physical Line("SURFACE") = {1};
Physical Surface("ALL_ELEMENTS") = {1};
