import pandas as pd
import numpy as np
import os
from argiope.abq.pypostproc import read_field_report as rfr
from argiope.mesh import read_h5, Field, write_xdmf_inc

simName= "${simName}"
sample_mesh   = read_h5("outputs/{0}_sample_mesh.h5".format(simName))


# FILES LISTING
files = os.listdir("reports/")

# INIT
nb_frame=-1

for path in files:
#HISTORY OUTPUTS

# FIELD OUTPUTS
  if path.endswith(".frpt"):
    print "#LOADING: " + path
    #instance = path.split("instance")[1][1:].split("_step-")[0]
    simName_Verif, d    = path.split("_instance-")
    instance, d = d.split("_step-")
    sname, d    = d.split("_frame-")
    frame, d    = d.split("_var-")
    frame = int(frame) # generate a warning    

#    print frame
    field_short_name =  d[:-5]
    nb_frame = max(nb_frame,int(frame))    
    
    info = {"tag": path[:-5], "position": "Nodal", "frame": frame , "field_short_name": field_short_name, "path": path}
    data = rfr("reports/" + path)
    field = Field(info, data)
    if instance == "I_SAMPLE":
      sample_mesh.add_field(tag = info["tag"], field = field)
      
   

sample_mesh.save()
for i in range(nb_frame):
    print "#WRINTING XML FILE of frame : " + str(i) 
    write_xdmf_inc(sample_mesh,
           "outputs/{0}_sample_mesh".format(simName), 
           dataformat = "XML",
           curent_frame = i)
         

