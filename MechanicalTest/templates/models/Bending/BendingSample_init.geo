// Gmsh project created on Tue Jul 04 13:45:43 2017
lc1=0.1;
Nx=50+1;
Ny=20+1;
L=6;
h=2;
Point(1) = {0, 0, 0, lc1};
Point(2) = {L, 0, 0, lc1};
Point(3) = {L, h, 0, lc1};
Point(4) = {0, h, 0, lc1};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Transfinite Line {1,3} = Nx;
Transfinite Line {2,4} = Ny;
Line Loop(1) = {1, 2, 3, 4};
Plane Surface(1) = {1};
Transfinite Surface {1};
