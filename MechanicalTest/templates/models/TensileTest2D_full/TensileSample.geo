// Gmsh project created on Sat Nov 05 19:09:59 2016

L = $L;
l =  $l;
r =  $r;

lcf = $lcf;
lcg = $lcg;

Point(1) = {0. ,  0. , 0., lcf};
Point(2) = {l/2 ,  0. , 0., lcf};
Point(3) = {l/2 ,  L/2 , 0., lcf};
Point(4) = {l/2+r ,  L/2 , 0., lcg};
Point(5) = {l/2+r ,  L/2+r , 0., lcg};
Point(6) = {l/2+r ,  L/2+3*r , 0., lcg};
Point(7) = {0 ,  L/2+3*r , 0., lcg};


Line(1) = {2,3};
Circle(2) = {3, 4, 5};
Line(3) = {5,6};
Line(4) = {6,7};




Symmetry {1, 0, 0, 0} {
  Duplicata { Line{1, 2, 3, 4}; }
}
Symmetry {0, 1, 0, 0} {
  Duplicata { Line{1, 2, 3, 4, 5, 6, 7, 8}; }
}


Line Loop(1) = {1,2,3,4,-8,-7,-6,-5,13,14,15,16,-12,-11,-10,-9};
Plane Surface(1) = {1};

Recombine Surface {1};
Physical Line("SURFACE") = {1,2,3,5,6,7,9,10,11,13,14,15};
Physical Line("TOP") = {4,8};
Physical Line("BOT") = {12,16};
Physical Surface("ALL_ELEMENTS") = {1};